package ru.konovalov.tm.controller;

import ru.konovalov.tm.api.IProjectService;
import ru.konovalov.tm.api.IProjectTaskService;
import ru.konovalov.tm.api.ITaskController;
import ru.konovalov.tm.api.ITaskService;
import ru.konovalov.tm.enumerated.Sort;
import ru.konovalov.tm.enumerated.Status;
import ru.konovalov.tm.exeption.empty.EmptyNameException;
import ru.konovalov.tm.exeption.entity.TaskNotFoundException;
import ru.konovalov.tm.model.Task;
import ru.konovalov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    private final IProjectTaskService projectTaskService;

    private final IProjectService projectService;

    public TaskController(final ITaskService taskService, final IProjectTaskService projectTaskService, IProjectService projectService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
        this.projectService = projectService;
    }

    @Override
    public void showList() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Task> tasks;
        if (sort == null || sort.isEmpty()) tasks = taskService.findAll();
        else {
            final Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            tasks = taskService.findAll(sortType.getComparator());
        }
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    @Override
    public void create() {
        System.out.println("[TASK CREATE]");
        System.out.println("[ENTER NAME]:");
        final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.add(name, description);
        if (task == null) throw new TaskNotFoundException();
            return;
        }

    @Override
    public void clear() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + task.getStatus().getDisplayName());
    }

    @Override
    public void showTaskByName() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findOneByName(name);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[DELETE TASK]");
        System.out.println("ENTER INDEX");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.removeTaskByIndex(index);
        if (task == null)throw new TaskNotFoundException();
    }

    @Override
    public void removeTaskById() {
        System.out.println("[DELETE TASK]");
        System.out.println("ENTER ID");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeOneById(id);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void removeTaskByName() {
        System.out.println("[DELETE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.removeOneByName(name);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskByIndex(index, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateTaskById() {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskById(id, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void startTaskById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.startTaskById(id);
        if (task == null) System.out.println("[FAILED]");
        else System.out.println("[OK]");
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("[START TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.startTaskByIndex(index);
        if (task == null) System.out.println("[FAILED]");
        else System.out.println("[OK]");
    }

    @Override
    public void startTaskByName() {
        System.out.println("[START TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.startTaskByName(name);
        if (task == null) System.out.println("[FAILED]");
        else System.out.println("[OK]");
    }

    @Override
    public void finishTaskById() {
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER ID:");
        final String Id = TerminalUtil.nextLine();
        final Task task = taskService.finishTaskById(Id);
        if (task == null) System.out.println("[FAILED]");
        else System.out.println("[OK]");
    }

    @Override
    public void finishTaskByIndex() {
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER ID:");
        final String Id = TerminalUtil.nextLine();
        final Task task = taskService.finishTaskById(Id);
        if (task == null) System.out.println("[FAILED]");
        else System.out.println("[OK]");
    }

    @Override
    public void finishTaskByName() {
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.finishTaskByName(name);
        if (task == null) System.out.println("[FAILED]");
        else System.out.println("[OK]");
    }

    @Override
    public void changeTaskStatusById() {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = taskService.changeTaskStatusById(id, status);
        if (task == null) System.out.println("[FAILED]");
        else System.out.println("[OK]");
    }

    @Override
    public void changeTaskStatusByName() {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        final String statusName = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusName);
        final Task task = taskService.changeTaskStatusByName(name, status);
        if (task == null) System.out.println("[FAILED]");
        else System.out.println("[OK]");
    }

    @Override
    public void changeTaskStatusByIndex() {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = taskService.changeTaskStatusByIndex(index, status);
        if (task == null) System.out.println("[FAILED]");
        else System.out.println("[OK]");
    }

    @Override
    public void assignTaskByProjectId() {
        System.out.println("ASSIGNING TASK TO PROJECT");
        System.out.println("[project-list] command can help you if you don't know ID");
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID");
        System.out.println("[task-list] command can help you if you don't know ID");
        final String taskId = TerminalUtil.nextLine();
        final Task taskUpdt = projectTaskService.assignTaskByProjectId(projectId, taskId);
        if (taskUpdt == null) System.out.println("FAILED");
        else System.out.println("[TASK ASSIGNED TO PROJECT]");
    }

    @Override
    public void unassignTaskById() {
        System.out.println("[REMOVING TASK FROM PROJECT BY ID]");
        System.out.println("ENTER TASK ID:");
        System.out.println("[task-list] command can help you if you don't know ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = projectTaskService.unassignTaskByProjectId(taskId);
        if (task == null) System.out.println("[FAILED]");
        else System.out.println("[TASK DELETED FROM PROJECT]");
    }

    @Override
    public void removeAllTaskByProjectId() {
        System.out.println("[REMOVING ALL TASKS BY PROJECT ID]");
        System.out.println("ENTER TASK ID:");
        System.out.println("[task-list] command can help you if you don't know ID");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = projectTaskService.removeTasksByProjectId(projectId);
        if (tasks.isEmpty()) System.out.println("PROJECT CLEARED");
        else System.out.println("[FAILED]");
    }

    @Override
    public void showTaskByProjId() {
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> taskList = projectTaskService.findALLTaskByProjectId(projectId);
        if (taskList == null) System.out.println("[FAIL! NO SUCH PROJECT]");
        else if (taskList.size() < 1) System.out.println("[OK, NO TASKS]");
        else {
            for (Task task : taskList) showTask(task);
            System.out.println("[OK]");
        }
    }

}