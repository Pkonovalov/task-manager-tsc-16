package ru.konovalov.tm.controller;

import ru.konovalov.tm.api.IProjectController;
import ru.konovalov.tm.api.IProjectService;
import ru.konovalov.tm.api.IProjectTaskService;
import ru.konovalov.tm.enumerated.Sort;
import ru.konovalov.tm.enumerated.Status;
import ru.konovalov.tm.exeption.empty.EmptyNameException;
import ru.konovalov.tm.exeption.entity.ProjectNotFoundException;
import ru.konovalov.tm.model.Project;
import ru.konovalov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public ProjectController(final IProjectService projectService, IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void showList() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Project> projects;
        if (sort == null || sort.isEmpty()) projects = projectService.findAll();
        else {
            final Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            projects = projectService.findAll(sortType.getComparator());
        }
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
    }

    @Override
    public void create() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("[ENTER NAME]:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = (Project) projectService.add(name, description);
        if (project == null) throw new EmptyNameException();
    }

    @Override
    public void clear() {
        System.out.println("[PROJECT CLEAR]");
        projectService.clear();
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public void showProjectById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + project.getStatus().getDisplayName());
    }

    @Override
    public void showProjectByName() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[DELETE PROJECT]");
        System.out.println("ENTER INDEX");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.removeProjectByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void removeProjectById() {
        System.out.println("[DELETE PROJECT]");
        System.out.println("ENTER ID");
        final String id = TerminalUtil.nextLine();
        final Project project = projectTaskService.removeProjectById(id);
        if (project == null) System.out.println("[FAIL]");
    }

    @Override
    public void removeProjectByName() {
        System.out.println("[DELETE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.removeOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateProjectByIndex(index, name, description);
        if (projectUpdated == null) throw new ProjectNotFoundException();
    }

    @Override
    public void updateProjectById() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateProjectById(id, name, description);
        if (projectUpdated == null) throw new ProjectNotFoundException();
    }

    @Override
    public void startProjectById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.startProjectById(id);
        if (project == null) System.out.println("[FAILED]");
    }

    @Override
    public void startProjectByIndex() {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.startProjectByIndex(index);
        if (project == null) System.out.println("[FAILED]");
    }

    @Override
    public void startProjectByName() {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.startProjectByName(name);
        if (project == null) System.out.println("[FAILED]");
    }

    @Override
    public void finishProjectById() {
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER ID:");
        final String Id = TerminalUtil.nextLine();
        final Project project = projectService.finishProjectById(Id);
        if (project == null) System.out.println("[FAIL]");
    }

    @Override
    public void finishProjectByIndex() {
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.finishProjectByIndex(index);
        if (project == null) System.out.println("[FAILED]");
    }

    @Override
    public void finishProjectByName() {
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.finishProjectByName(name);
        if (project == null) System.out.println("[FAIL]");
    }

    @Override
    public void changeProjectStatusById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project project = projectService.changeProjectStatusById(id, status);
        if (project == null) System.out.println("[FAILED]");
    }

    @Override
    public void changeProjectStatusByName() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        final String statusName = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusName);
        final Project project = projectService.changeProjectStatusByName(name, status);
        if (project == null) System.out.println("[FAILED]");
    }

    @Override
    public void changeProjectStatusByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project project = projectService.changeProjectStatusByIndex(index, status);
        if (project == null) System.out.println("[FAILED]");
    }

}