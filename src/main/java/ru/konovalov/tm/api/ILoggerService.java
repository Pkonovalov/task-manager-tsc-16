package ru.konovalov.tm.api;

public interface ILoggerService {

    void info(String message);

    void command(String message);

    void error (Exception e);
}
