package ru.konovalov.tm.api;

import ru.konovalov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    List<Task> findALLTaskByProjectId(String projectId);

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    List<Task> removeAllTaskByProjectId(String projectId);

    Task assignTaskByProjectId(String projectId, String taskId);

    Task unassignTaskByProjectId(String taskId);

    void clear();

    Task findOneById(String id);

    Task removeOneById(String id);

    Task findOneByIndex(Integer index);

    Task removeOneByIndex(Integer index);

    Task findOneByName(String name);

    Task removeOneByName(String name);

}
