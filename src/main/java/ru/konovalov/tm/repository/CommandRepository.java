package ru.konovalov.tm.repository;

import ru.konovalov.tm.api.ICommandRepository;
import ru.konovalov.tm.constant.ArgumentConst;
import ru.konovalov.tm.constant.TerminalConst;
import ru.konovalov.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            TerminalConst.CMD_ABOUT, ArgumentConst.ARG_ABOUT, "Show developer info."
    );

    private static final Command HELP = new Command(
            TerminalConst.CMD_HELP, ArgumentConst.ARG_HELP, "Show terminal commands."
    );

    private static final Command VERSION = new Command(
            TerminalConst.CMD_VERSION, ArgumentConst.ARG_VERSION, "Show terminal commands."
    );

    private static final Command INFO = new Command(
            TerminalConst.CMD_INFO, ArgumentConst.ARG_INFO, "Show system info."
    );

    private static final Command EXIT = new Command(
            TerminalConst.CMD_EXIT, null, "Close application."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConst.CMD_ARGUMENTS, null, "Show program arguments."
    );

    private static final Command COMMANDS = new Command(
            TerminalConst.CMD_COMMANDS, null, "Show program commands."
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.CMD_TASK_CREATE, null, "Create new task."
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.CMD_TASK_CLEAR, null, "Clear all tasks."
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.CMD_TASK_LIST, null, "Show task list."
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.CMD_PROJECT_CREATE, null, "Create new project."
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.CMD_PROJECT_CLEAR, null, "Clear all project."
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.CMD_PROJECT_LIST, null, "Show project list."
    );

    private static final Command TASK_VIEW_BY_ID = new Command(
            TerminalConst.CMD_TASK_VIEW_BY_ID, null, "Find and show task by ID"
    );

    private static final Command TASK_VIEW_BY_INDEX = new Command(
            TerminalConst.CMD_TASK_VIEW_BY_INDEX, null, "Find and show task by index"
    );

    private static final Command TASK_VIEW_BY_NAME = new Command(
            TerminalConst.CMD_TASK_VIEW_BY_NAME, null, "Find and show task by name"
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.CMD_TASK_UPDATE_BY_ID, null, "Find and update task by ID"
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.CMD_TASK_UPDATE_BY_INDEX, null, "Find and update task by index"
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.CMD_TASK_REMOVE_BY_ID, null, "Find and delete task by ID"
    );

    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.CMD_TASK_REMOVE_BY_INDEX, null, "Find and delete task by index"
    );

    private static final Command TASK_REMOVE_BY_NAME = new Command(
            TerminalConst.CMD_TASK_REMOVE_BY_NAME, null, "Find and delete task by name"
    );

    private static final Command PROJECT_VIEW_BY_ID = new Command(
            TerminalConst.CMD_PROJECT_VIEW_BY_ID, null, "Find and show project by ID"
    );

    private static final Command PROJECT_VIEW_BY_INDEX = new Command(
            TerminalConst.CMD_PROJECT_VIEW_BY_INDEX, null, "Find and show project by index"
    );

    private static final Command PROJECT_VIEW_BY_NAME = new Command(
            TerminalConst.CMD_PROJECT_VIEW_BY_NAME, null, "Find and show project by name"
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.CMD_PROJECT_UPDATE_BY_ID, null, "Find and update project by ID"
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.CMD_PROJECT_UPDATE_BY_INDEX, null, "Find and update project by index"
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.CMD_PROJECT_REMOVE_BY_ID, null, "Find and delete project by ID"
    );

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.CMD_PROJECT_REMOVE_BY_INDEX, null, "Find and delete project by index"
    );

    private static final Command PROJECT_REMOVE_BY_NAME = new Command(
            TerminalConst.CMD_PROJECT_REMOVE_BY_NAME, null, "Find and delete project by name"
    );

    private static final Command PROJECT_START_BY_ID = new Command(
            TerminalConst.CMD_PROJECT_START_BY_ID, null, "Start project by id"
    );

    private static final Command PROJECT_START_BY_INDEX = new Command(
            TerminalConst.CMD_PROJECT_START_BY_INDEX, null, "Start project by index"
    );

    private static final Command PROJECT_START_BY_NAME = new Command(
            TerminalConst.CMD_PROJECT_START_BY_NAME, null, "Start project by name"
    );

    private static final Command TASK_FINISH_BY_ID = new Command(
            TerminalConst.CMD_TASK_FINISH_BY_ID, null, "Finish task by id"
    );

    private static final Command TASK_FINISH_BY_INDEX = new Command(
            TerminalConst.CMD_TASK_FINISH_BY_INDEX, null, "Finish task by index"
    );

    private static final Command TASK_FINISH_BY_NAME = new Command(
            TerminalConst.CMD_TASK_FINISH_BY_NAME, null, "Finish task by name"
    );

    private static final Command PROJECT_FINISH_BY_ID = new Command(
            TerminalConst.CMD_PROJECT_FINISH_BY_ID, null, "Finish project by id"
    );

    private static final Command PROJECT_FINISH_BY_INDEX = new Command(
            TerminalConst.CMD_PROJECT_FINISH_BY_INDEX, null, "Finish project by index"
    );

    private static final Command PROJECT_FINISH_BY_NAME = new Command(
            TerminalConst.CMD_PROJECT_FINISH_BY_NAME, null, "Finish project by name"
    );

    private static final Command PROJECT_STATUS_CHANGE_BY_ID = new Command(
            TerminalConst.CMD_PROJECT_STATUS_CHANGE_BY_ID, null, "Change project status by Id"
    );

    private static final Command PROJECT_STATUS_CHANGE_BY_NAME = new Command(
            TerminalConst.CMD_PROJECT_STATUS_CHANGE_BY_NAME, null, "Change project status by name"
    );

    private static final Command PROJECT_STATUS_CHANGE_BY_INDEX = new Command(
            TerminalConst.CMD_PROJECT_STATUS_CHANGE_BY_INDEX, null, "Change project status by index"
    );

    private static final Command TASK_STATUS_CHANGE_BY_ID = new Command(
            TerminalConst.CMD_TASK_STATUS_CHANGE_BY_ID, null, "Change task status by Id"
    );

    private static final Command TASK_STATUS_CHANGE_BY_NAME = new Command(
            TerminalConst.CMD_TASK_STATUS_CHANGE_BY_NAME, null, "Change task status by name"
    );

    private static final Command TASK_STATUS_CHANGE_BY_INDEX = new Command(
            TerminalConst.CMD_TASK_STATUS_CHANGE_BY_INDEX, null, "Change task status by index"
    );

    private static final Command TASK_ASSIGN_TO_PROJECT = new Command(
            TerminalConst.CMD_TASK_ASSIGN_TO_PROJECT, null, "Assign task to project"
    );

    private static final Command TASK_UNASSIGN_TO_PROJECT = new Command(
            TerminalConst.CMD_TASK_UNASSIGN_FROM_PROJECT, null, "Unassign task to project"
    );

    private static final Command TASK_ALL_REMOVE_FROM_PROJECT = new Command(
            TerminalConst.CMD_TASK_ALL_REMOVE_FROM_PROJECT, null, "Remove all task from project"
    );

    private static final Command TASK_LIST_BY_PROJECT_ID = new Command(
            TerminalConst.CMD_TASK_LIST_BY_PROJECT_ID, null, "Show task by project ID"
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            ABOUT, HELP, VERSION, INFO, ARGUMENTS, COMMANDS,
            TASK_CLEAR, TASK_CREATE, TASK_LIST,
            PROJECT_CLEAR, PROJECT_CREATE, PROJECT_LIST,
            TASK_REMOVE_BY_NAME, TASK_REMOVE_BY_INDEX, TASK_REMOVE_BY_ID, TASK_UPDATE_BY_INDEX, TASK_UPDATE_BY_ID,
            TASK_VIEW_BY_NAME, TASK_VIEW_BY_INDEX, TASK_VIEW_BY_ID,
            PROJECT_REMOVE_BY_NAME, PROJECT_REMOVE_BY_INDEX, PROJECT_REMOVE_BY_ID, PROJECT_UPDATE_BY_INDEX, PROJECT_UPDATE_BY_ID,
            PROJECT_VIEW_BY_NAME, PROJECT_VIEW_BY_INDEX, PROJECT_VIEW_BY_ID,
            PROJECT_START_BY_NAME, PROJECT_START_BY_INDEX, PROJECT_START_BY_ID,
            TASK_STATUS_CHANGE_BY_ID, TASK_STATUS_CHANGE_BY_INDEX, TASK_STATUS_CHANGE_BY_NAME,
            TASK_FINISH_BY_NAME, TASK_FINISH_BY_INDEX, TASK_FINISH_BY_ID,
            PROJECT_FINISH_BY_NAME, PROJECT_FINISH_BY_INDEX, PROJECT_FINISH_BY_ID,
            PROJECT_STATUS_CHANGE_BY_ID, PROJECT_STATUS_CHANGE_BY_INDEX, PROJECT_STATUS_CHANGE_BY_NAME,
            TASK_ASSIGN_TO_PROJECT, TASK_UNASSIGN_TO_PROJECT, TASK_ALL_REMOVE_FROM_PROJECT, TASK_LIST_BY_PROJECT_ID,
            EXIT
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}