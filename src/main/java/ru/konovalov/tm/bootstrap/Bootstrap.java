package ru.konovalov.tm.bootstrap;

import ru.konovalov.tm.api.*;
import ru.konovalov.tm.constant.ArgumentConst;
import ru.konovalov.tm.constant.TerminalConst;
import ru.konovalov.tm.controller.CommandController;
import ru.konovalov.tm.controller.ProjectController;
import ru.konovalov.tm.controller.TaskController;
import ru.konovalov.tm.exeption.system.UnknownArgumentException;
import ru.konovalov.tm.exeption.system.UnknownCommandException;
import ru.konovalov.tm.repository.CommandRepository;
import ru.konovalov.tm.repository.ProjectRepository;
import ru.konovalov.tm.repository.TaskRepository;
import ru.konovalov.tm.service.*;
import ru.konovalov.tm.util.TerminalUtil;

public class Bootstrap {
    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService, projectTaskService, projectService);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final ILoggerService loggerService = new LoggerService();


    public void run(final String[] args) {
        loggerService.info("*** WeLcOmE To TaSk MaNaGeR ***");
        if (parseArgs(args)) System.exit(0);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
                parseCommand(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAILED]");
            }
        }
    }

    public void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.ARG_HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ARG_VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ARG_INFO:
                commandController.showSystemInfo();
                break;
            default:
                throw new UnknownArgumentException();
        }
    }

    public void parseCommand(final String command) {
        if (command == null) return;
        switch (command) {
            case TerminalConst.CMD_ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.CMD_HELP:
                commandController.showHelp();
                break;
            case TerminalConst.CMD_VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.CMD_INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.CMD_COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.CMD_EXIT:
                commandController.exit();
                break;
            case TerminalConst.CMD_TASK_LIST:
                taskController.showList();
                break;
            case TerminalConst.CMD_TASK_CREATE:
                taskController.create();
                break;
            case TerminalConst.CMD_TASK_CLEAR:
                taskController.clear();
                break;
            case TerminalConst.CMD_PROJECT_LIST:
                projectController.showList();
                break;
            case TerminalConst.CMD_PROJECT_CREATE:
                projectController.create();
                break;
            case TerminalConst.CMD_PROJECT_CLEAR:
                projectController.clear();
                break;
            case TerminalConst.CMD_TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.CMD_TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.CMD_TASK_REMOVE_BY_NAME:
                taskController.removeTaskByName();
                break;
            case TerminalConst.CMD_TASK_VIEW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConst.CMD_TASK_VIEW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConst.CMD_TASK_VIEW_BY_NAME:
                taskController.showTaskByName();
                break;
            case TerminalConst.CMD_TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.CMD_TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.CMD_PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConst.CMD_PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.CMD_PROJECT_REMOVE_BY_NAME:
                projectController.removeProjectByName();
                break;
            case TerminalConst.CMD_PROJECT_VIEW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.CMD_PROJECT_VIEW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.CMD_PROJECT_VIEW_BY_NAME:
                projectController.showProjectByName();
                break;
            case TerminalConst.CMD_PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.CMD_PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.CMD_PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case TerminalConst.CMD_PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case TerminalConst.CMD_PROJECT_START_BY_NAME:
                projectController.startProjectByName();
                break;
            case TerminalConst.CMD_PROJECT_FINISH_BY_ID:
                projectController.finishProjectById();
                break;
            case TerminalConst.CMD_PROJECT_FINISH_BY_INDEX:
                projectController.finishProjectByIndex();
                break;
            case TerminalConst.CMD_PROJECT_FINISH_BY_NAME:
                projectController.finishProjectByName();
                break;
            case TerminalConst.CMD_TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case TerminalConst.CMD_TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case TerminalConst.CMD_TASK_START_BY_NAME:
                taskController.startTaskByName();
                break;
            case TerminalConst.CMD_TASK_FINISH_BY_ID:
                taskController.finishTaskById();
                break;
            case TerminalConst.CMD_TASK_FINISH_BY_INDEX:
                taskController.finishTaskByIndex();
                break;
            case TerminalConst.CMD_TASK_FINISH_BY_NAME:
                taskController.finishTaskByName();
                break;
            case TerminalConst.CMD_PROJECT_STATUS_CHANGE_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case TerminalConst.CMD_PROJECT_STATUS_CHANGE_BY_NAME:
                projectController.changeProjectStatusByName();
                break;
            case TerminalConst.CMD_PROJECT_STATUS_CHANGE_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case TerminalConst.CMD_TASK_STATUS_CHANGE_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case TerminalConst.CMD_TASK_STATUS_CHANGE_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case TerminalConst.CMD_TASK_STATUS_CHANGE_BY_NAME:
                taskController.changeTaskStatusByName();
                break;
            case TerminalConst.CMD_TASK_ASSIGN_TO_PROJECT:
                taskController.assignTaskByProjectId();
                break;
            case TerminalConst.CMD_TASK_UNASSIGN_FROM_PROJECT:
                taskController.unassignTaskById();
                break;
            case TerminalConst.CMD_TASK_ALL_REMOVE_FROM_PROJECT:
                taskController.removeAllTaskByProjectId();
                break;
            case TerminalConst.CMD_TASK_LIST_BY_PROJECT_ID:
                taskController.showTaskByProjId();
            default:
                throw new UnknownCommandException(command);
        }
    }

    public boolean parseArgs(String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

}